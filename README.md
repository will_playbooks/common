# Common

A ansible role for baseline tasks

## Requirements

Ansible installed

## Default Variables

### General

**hostname:** ansible.localdomain

### NTP

**ntpserver1:** 0.pool.ntp.org  
**ntpserver2:** 1.pool.ntp.org  

### Networking

**nic_management:** eth0  
**nic_management_boot:** "yes"  
**nic_management_ip:** 192.168.198.100  
**nic_management_prefix:** 24  
**nic_management_gw:** 192.168.198.254  
**nic_storage:** eth1  
**nic_storage_boot:** "yes"  
**nic_storage_ip:** 192.168.199.100  
**nic_storage_prefix:** 24  
**bridge_name:** br0  
**bridge_boot:** "yes"  
**bridge_ip:** 192.168.198.100  
**bridge_prefix:** 24  
**bridge_gw:** 192.168.198.254  


## Dependencies

None

## Example Playbook


    - hosts: servers
      roles:
         - { role: common }

## License

BSD